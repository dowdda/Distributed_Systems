require 'socket'
require 'thread/pool'
SIZE = 1024*1024

files = Array.new

pool = Thread.pool(8)

ip = IPSocket.getaddress(Socket.gethostname)
server = TCPServer.open(ip, 8004)
print "Here We GO!!!\n"

print ip + "\n"

loop do
  if pool.idle?
    pool.process(server.accept) do |socket|
      print "\n\n"
      request = socket.gets
      print request
      update = false
      name = socket.gets
      name = name[0...-1]
      if request == "write\n"
        files.each do |item|
          if name == item
            update = true
            socket.print "file update"
          end
        end
        if update == false
          socket.print "new file"
        end
      elsif request == "read\n"
          files.each do |item|
            if name == item
              socket.print "file read"
              update = true
            end
          end
          if update == false
            socket.print "no file"
          end
      end
      socket.close
    end
  end
end
