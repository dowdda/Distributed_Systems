require 'socket'
require 'thread/pool'
SIZE = 1024*1024

pool = Thread.pool(8)

ip = IPSocket.getaddress(Socket.gethostname)
server = TCPServer.open(ip, 8002)
print "Here We GO!!!\n"

print ip + "\n"

cache = Array.new
cache_file = Array.new

loop do
  if pool.idle?
    pool.process(server.accept) do |socket|
      print "\n\n"
      request = socket.gets
      print request
      found = false
      if request == "cache\n"
        file = socket.gets
        file = file[0...-1]
        cache_file.each_with_index do |item, index|
          if item == file
            socket.print "found"
            found = true
            socket.print cache[index]
          end
        end
        if found == false
          socket.print "not found"
        end
      elsif request == "add\n"
        file = socket.gets
        data = ""
        while chunk = socket.read(SIZE)
          data = data + chunk
        end
        cache_file << file
        cache << data
        print cache_file
      elsif request == "delete\n"
        file = socket.gets
        file = file[0...-1]
        cache_file.each_with_index do |item, index|
          if item == file
            found = true
            cache_file.delete_at(index)
            cache.delete_at(index)
            socket.print "deleted"
          end
        end
        if found == false
          socket.print "not found"
        end
      end
      socket.close
    end
  end
end
