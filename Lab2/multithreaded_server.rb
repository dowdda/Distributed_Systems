require 'socket'
require 'thread/pool'

pool = Thread.pool(8)
port = ARGV[0]

ip = IPSocket.getaddress(Socket.gethostname)
server = TCPServer.open(ip, port)
print "Here We GO!!!\n"

print ip + "\n"

loop do
  if pool.idle?
    pool.process(server.accept) do |socket|
      print "\n\n"

      request = socket.gets

      print request
      if request == "KILL_SERVICE\n"
        socket.close
        exit!
      end

      if request.start_with?('HELO')
        response = "#{request}IP: #{ip}\nPORT: #{port}\nStudentID: 8d80a9b621cedc04990a4d42de7941f319f6ade9140528fdeebac221836fadd0\n"
        print response
        socket.print response
      end
      socket.close
    end
  end
end
